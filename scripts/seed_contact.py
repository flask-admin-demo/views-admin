def create_app(release_level):
    app = Flask(__name__)
    if release_level and release_level in EnvironmentMapper:
        app.config.from_object(EnvironmentMapper[release_level])

    db.init_app(app)
    return app


if __name__ == "__main__":
    import os
    from faker import Faker
    from flask import Flask
    from configuration.database import db
    from configuration import EnvironmentMapper
    from database_models.contact import Contact
    from database_models.person import Person

    release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()
    app = create_app(release_level)
    fake = Faker('en_US')

    with app.app_context():
        persons = db.session.query(Person).all()
        number_of_persons = len(persons)
        max_connections_per_person = fake.random_int(min=0, max=int(number_of_persons*.3))

        if number_of_persons > 0:
            for person in persons:
                connections_added = 0
                number_of_connections = fake.random_int(min=0, max=max_connections_per_person)
                if number_of_connections == 0:
                    print("Added 0 connections for Person(%s) %s %s" % (str(person.id), person.first_name, person.last_name))
                    continue
                connection_indices = set([fake.random_int(min=0, max=number_of_persons) for i in range(number_of_connections)])
                for index in connection_indices:
                    try:
                        connection_person = persons[index]
                        if connection_person.id == person.id:
                            continue
                        new_connection = Contact(
                            first_person_id=person.id,
                            second_person_id=connection_person.id,
                            status=fake.random_int(min=0, max=4),
                            created=fake.past_datetime(start_date="-30d", tzinfo=None).isoformat() + "Z",
                            modified=fake.past_datetime(start_date="-10d", tzinfo=None).isoformat() + "Z"
                        )
                        db.session.add(new_connection)
                        db.session.commit()
                        connections_added += 1
                    except Exception as e:
                        print(e)
                        db.session.rollback()

                print("Added %d connections for Person(%s) %s %s" % (connections_added, str(person.id), person.first_name, person.last_name))
