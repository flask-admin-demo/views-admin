
def create_app(release_level):
    app = Flask(__name__)
    if release_level and release_level in EnvironmentMapper:
        app.config.from_object(EnvironmentMapper[release_level])

    db.init_app(app)
    return app


if __name__ == "__main__":
    import os
    from faker import Faker
    from flask import Flask
    from configuration.database import db
    from configuration import EnvironmentMapper
    from database_models.person import Person

    release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()
    app = create_app(release_level)
    fake = Faker('en_US')
    with app.app_context():
        number_of_persons = fake.random_int(min=50, max=1000)
        current_person = 0
        added_persons = 0
        while current_person < number_of_persons:
            try:
                profile = fake.simple_profile()
                new_person = Person(
                    id=fake.uuid4(),
                    first_name=fake.first_name(),
                    last_name=fake.last_name(),
                    email=profile["mail"],
                    phone="+1555"+str(fake.random_int(min=1000000, max=9999999)),
                    birthday=profile["birthdate"],
                    created=fake.past_datetime(start_date="-30d", tzinfo=None).isoformat() + "Z",
                    modified=fake.past_datetime(start_date="-10d", tzinfo=None).isoformat() + "Z"
                )
                db.session.add(new_person)
                db.session.commit()
                added_persons += 1
                current_person += 1
            except Exception as e:
                print(e)
                db.session.rollback()
                current_person += 1

    print("Created %d persons" % added_persons)
