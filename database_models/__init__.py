from configuration.database import db

__schema__ = "public"


class GenericDatabaseModel(db.Model):
    __abstract__ = True
    __table_args__ = {'schema': __schema__}