from datetime import datetime

from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.dialects.postgresql import UUID
from database_models import GenericDatabaseModel

# N.B. Status
# 0 = Neither connected
# 1 = second person connected
# 2 = first person connected
# 3 = both connected


class Contact(GenericDatabaseModel):
    __tablename__ = "contacts"

    first_person_id = Column(UUID(as_uuid=True), primary_key=True, nullable=False)
    second_person_id = Column(UUID(as_uuid=True), primary_key=True, nullable=False)
    status = Column(Integer(), nullable=False, default=0)
    created = Column(DateTime(), nullable=False, default=datetime.utcnow)
    modified = Column(DateTime(), nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
