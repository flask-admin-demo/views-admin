from datetime import datetime

from sqlalchemy import Column, String, DateTime, Text, Date
from sqlalchemy.dialects.postgresql import UUID

from database_models import GenericDatabaseModel


class Person(GenericDatabaseModel):
    __tablename__ = "persons"

    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False)
    first_name = Column(String(16), nullable=False)
    last_name = Column(String(32), nullable=False)
    email = Column(String(64), unique=True, nullable=False)
    phone = Column(String(16), unique=True, nullable=False)
    photo = Column(Text(), nullable=True)
    birthday = Column(Date(), nullable=True)
    created = Column(DateTime(), nullable=False, default=datetime.utcnow)
    modified = Column(DateTime(), nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
