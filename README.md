# Flask-Admin Demo

## BasicAdmin 
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/basic-admin.git`
2. `$ cd basic-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. Configure Postgres by creating `flask_admin_demo_postgres` user and the `flask_admin_demo` database
6. Make sure the `flask_admin_demo_postgres` with password are saved in the .env file for the SQLALCHEMY_DATABASE_URI variable
7. `$ ./bin/flask db upgrade`

### Swatches

There are many swatches you can use from [Bootswatch](https://bootswatch.com/3/). These swatches are based off of Twitter [Bootstrap3](https://getbootstrap.com/docs/3.3/) and can be customised.

If, you want to create your own swatch, you may do so, but that is outside the scope of this discussion. 
### Our First View
All we have to do is link the database model with the view model!


## ViewsAdmin (This Project!)
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/views-admin.git`
2. `$ cd views-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. `$ ./bin/flask db upgrade`

### Grouping Views

You can group model views using categories.

You can create subcategories.

### Customizing

You can find a full list of customizations [here](https://flask-admin.readthedocs.io/en/latest/api/mod_model/#flask_admin.model.BaseModelView)

### Jinja2 and Standalone Views

1. Create your pages in /templates
2. Create your views in /views
3. Load your views into the Admin App

## RowsAdmin
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/row-admin.git`
2. `$ cd row-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. `$ ./bin/flask db upgrade`

### Reordering Columns
### Glyphicon is your Friend
### Customizing Batch Actions

## FormsAdmin
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/forms-admin.git`
2. `$ cd forms-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. `$ ./bin/flask db upgrade`

### Basic Form
### Custom Form
### CSRF Protection

## AdvancedFormAdmin
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/advanced-forms-admin.git`
2. `$ cd advanced-forms-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. `$ ./bin/flask db upgrade`

### WYSIWIG Text Forms
### File Upload
### Rules
### Overriding the Scaffolding

## FiltersAdmin
### Setup
1. `$ git clone git@gitlab.com:flask-admin-demo/filter-admin.git`
2. `$ cd filter-admin` 
3. `$ python3 -m venv venv`
4. `$ pip3 install -r requirements.txt`
5. `$ ./bin/flask db upgrade`

### Extending BaseModelView 
### Basic Filter
### Linking Filters