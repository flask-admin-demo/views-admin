from flask_admin import Admin
from flask_admin.menu import MenuLink
from flask_sqlalchemy import SQLAlchemy

from database_models.contact import Contact
from database_models.person import Person
from views.contact import ContactView
from views.person import PersonView
from views.hello import HelloView


def add_views(admin_app: Admin, db: SQLAlchemy):
    # Adding Custom Views
    admin_app.add_view(PersonView(Person, db.session))
    admin_app.add_view(ContactView(Contact, db.session))
    # Adding Standalone Views
    admin_app.add_view(HelloView(name="Hello", endpoint="hello"))
    # Adding Standalone Links
    admin_app.add_link(MenuLink(name='Info', url='/', category="Info"))
    
    admin_app.add_sub_category(name="Libraries", parent_name="Info")
    admin_app.add_link(MenuLink(name='Faker', url='https://faker.readthedocs.io/en/master/locales/en_US.html', category='Libraries', target="_blank"))
    admin_app.add_link(MenuLink(name='Flask Admin', url='https://flask-admin.readthedocs.io/en/latest/', category='Libraries', target="_blank"))
    admin_app.add_link(MenuLink(name='Flask SQLAlchemy', url='https://flask-sqlalchemy.palletsprojects.com', category='Libraries', target="_blank"))
    admin_app.add_link(MenuLink(name='Flask Migrate', url='https://flask-migrate.readthedocs.io/en/latest/', category='Libraries', target="_blank"))
    
    admin_app.add_sub_category(name="Repositories", parent_name="Info")
    admin_app.add_link(MenuLink(name='BasicAdmin', url='https://gitlab.com/flask-admin-demo/basic-admin', category='Repositories', target="_blank"))
    admin_app.add_link(MenuLink(name='ViewsAdmin', url='https://gitlab.com/flask-admin-demo/views-admin', category='Repositories', target="_blank"))
    admin_app.add_link(MenuLink(name='RowsAdmin', url='https://gitlab.com/flask-admin-demo/rows-admin', category='Repositories', target="_blank"))
    admin_app.add_link(MenuLink(name='FormsAdmin', url='https://gitlab.com/flask-admin-demo/forms-admin', category='Repositories', target="_blank"))
    admin_app.add_link(MenuLink(name='AdvancedFormsAdmin', url='https://gitlab.com/flask-admin-demo/advanced-forms-admin', category='Repositories', target="_blank"))
    admin_app.add_link(MenuLink(name='FiltersAdmin', url='https://gitlab.com/flask-admin-demo/filters-admin', category='Repositories', target="_blank"))