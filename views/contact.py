from flask_admin.contrib.sqla import ModelView


class ContactView(ModelView):
    column_display_pk = True
    column_exclude_list = ("created", "modified")
    column_default_sort = [("modified", True)]
