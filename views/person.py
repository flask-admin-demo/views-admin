from flask_admin.contrib.sqla import ModelView


class PersonView(ModelView):
    can_edit = True
    can_create = True
    can_delete = False
    column_list = ("id", "first_name", "last_name", "email", "phone", "birthday")
    column_exclude_list = ("created", "modified")
    column_labels = dict(id="Person Id", first_name="First Name", last_name="Last Name")
    column_display_pk = True
    column_sortable_list = ("id", "last_name", "birthday")
    column_searchable_list = ("id", "last_name", "email", "phone", "birthday")
    column_default_sort = [("created", True)]
    page_size = 50
    can_set_page_size = True
    can_export = True
