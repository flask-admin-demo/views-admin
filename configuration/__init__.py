import os
import logging
from dotenv import find_dotenv, load_dotenv


basedir = os.path.abspath(os.path.dirname(__file__))
__appname__ = "flask_admin_demo"
release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()


ENV_FILE = find_dotenv()

if os.path.isfile(ENV_FILE + ".%s" % release_level):
    ENV_FILE = ENV_FILE + ".%s" % release_level

if ENV_FILE:
    load_dotenv(ENV_FILE)


class BaseConfig(object):
    APP_NAME = __appname__
    RELEASE_LEVEL = release_level
    APP_LOG_NAME = "{}-{}".format(release_level.lower(), APP_NAME)
    LOG_NAMESPACES = [__appname__]
    LOG_DIR = os.path.join(basedir, "logs")
    LOG_NAME = __appname__ + ".log"
    LOG_LEVEL = logging.DEBUG
    DEBUG = False
    SECRET_KEY = os.environ.get("FAD_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CSRF_HEADER = 'X-CSRFToken'
    CSRF_FIELD_NAME = 'csrftoken'
    FLASK_ADMIN_SWATCH = 'simplex'


class LocalConfig(BaseConfig):
    DEBUG = True
    NUM_PROXIES = 1
    SQLALCHEMY_ECHO = os.environ.get("SQLALCHEMY_ECHO", "false").lower() == "true"


class TestConfig(BaseConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get("TEST_DATABASE_URI")
    SQLALCHEMY_ECHO = os.environ.get("SQLALCHEMY_ECHO", "false").lower() == "true"


class ServerConfig(BaseConfig):
    LOG_LEVEL = logging.INFO
    SQLALCHEMY_ECHO = False


class StagingConfig(ServerConfig):
    DEBUG = True


class ProductionConfig(ServerConfig):
    pass


EnvironmentMapper = {
    "test": TestConfig(),
    "local": LocalConfig(),
    "staging": StagingConfig(),
    "prod": ProductionConfig()
}
