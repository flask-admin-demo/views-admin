import logging
import os

from flask import Flask
from flask_admin import Admin
from flask_admin.menu import MenuLink

from configuration import EnvironmentMapper
from configuration.database import db
from configuration.extensions import migrate


logger = logging.getLogger(__name__)
PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()
app = Flask(__name__, instance_relative_config=True)

# Pro-tip: centralize your configurations
if release_level and release_level in EnvironmentMapper:
    app.config.from_object(EnvironmentMapper[release_level])

# !!!! OVERRIDE !!!
# N.B. Checkout: https://bootswatch.com/3/
app.config['FLASK_ADMIN_SWATCH'] = 'darkly'

# DB Setup
db.init_app(app=app)
migrate.init_app(app=app, db=db)


# Setup Flask-Admin
# N.B. uncomment out for default /admin endpoint
# admin = Admin(app=app, name="BasicAdmin", template_mode="bootstrap3")
admin = Admin(app=app, name="ViewsAdmin", template_mode="bootstrap3", url="/")

from views import add_views

add_views(admin, db)


if __name__ == '__main__':
    app.run()
