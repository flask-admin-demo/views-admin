import logging
import os

from flask import Flask
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink

from configuration import EnvironmentMapper
from configuration.database import db
from configuration.extensions import migrate
from database_models.contact import Contact
from database_models.person import Person

logger = logging.getLogger(__name__)
PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))
release_level = os.getenv('FAD_RELEASE_LEVEL', 'local').lower()

app = Flask(__name__, instance_relative_config=True)

# Pro-tip: centralize your configurations
if release_level and release_level in EnvironmentMapper:
    app.config.from_object(EnvironmentMapper[release_level])

# !!!! OVERRIDE !!!
# N.B. Checkout: https://bootswatch.com/3/
app.config['FLASK_ADMIN_SWATCH'] = 'darkly'

# DB Setup
db.init_app(app=app)
migrate.init_app(app=app, db=db)


# Setup Flask-Admin
# N.B. uncomment out for default /admin endpoint
# admin = Admin(app=app, name="BasicAdmin", template_mode="bootstrap3")
admin = Admin(app=app, name="ViewsAdmin", template_mode="bootstrap3", url="/")

# Grouping Version 1
admin.add_view(ModelView(Person, db.session, category="Persons"))
admin.add_view(ModelView(Contact, db.session, category="Persons"))

# Grouping Version 2
admin.add_link(MenuLink(name='Info', url='/', category="Info"))

admin.add_sub_category(name="Libraries", parent_name="Info")
admin.add_link(MenuLink(name='Faker', url='https://faker.readthedocs.io/en/master/locales/en_US.html', category='Libraries', target="_blank"))
admin.add_link(MenuLink(name='Flask Admin', url='https://flask-admin.readthedocs.io/en/latest/', category='Libraries', target="_blank"))
admin.add_link(MenuLink(name='Flask SQLAlchemy', url='https://flask-sqlalchemy.palletsprojects.com', category='Libraries', target="_blank"))
admin.add_link(MenuLink(name='Flask Migrate', url='https://flask-migrate.readthedocs.io/en/latest/', category='Libraries', target="_blank"))

admin.add_sub_category(name="Repositories", parent_name="Info")
admin.add_link(MenuLink(name='BasicAdmin', url='https://gitlab.com/flask-admin-demo/basic-admin', category='Repositories', target="_blank"))
admin.add_link(MenuLink(name='ViewsAdmin', url='https://gitlab.com/flask-admin-demo/views-admin', category='Repositories', target="_blank"))
admin.add_link(MenuLink(name='RowsAdmin', url='https://gitlab.com/flask-admin-demo/rows-admin', category='Repositories', target="_blank"))
admin.add_link(MenuLink(name='FormsAdmin', url='https://gitlab.com/flask-admin-demo/forms-admin', category='Repositories', target="_blank"))
admin.add_link(MenuLink(name='AdvancedFormsAdmin', url='https://gitlab.com/flask-admin-demo/advanced-forms-admin', category='Repositories', target="_blank"))
admin.add_link(MenuLink(name='FiltersAdmin', url='https://gitlab.com/flask-admin-demo/filters-admin', category='Repositories', target="_blank"))


if __name__ == '__main__':
    app.run()
